# Ce momento contient les commandes indispensables pour utiliser Github </h3>
# voir le wiki pour plus de tutoriels

> Les commandes d'initialisation de projet : 

> > Cloner un projet depuis le serveur : 
<pre><code>
git clone < url-projet-distant > < dossier-a-creer > (copier un projet depuis un dépot distant vers un dossier local)
</code></pre>

> > Initialiser un projet local : 
<pre><code>
git init (initialiser un dépot Git dans le dossier local en cours)
git remote add origin < url-projet-distant > (connecter son dossier local en cours à un dépot distant)
</code></pre>

> Spécifier un dépot distant par défaut (default remote repository) :</h4> 

> > en tapant les deux lignes de commande suivantes :
<pre><code>
<ol>
<li>git config add branch.master.remote origin </li>
<li>git config branch.master.merge refs/heads/master</li>
</ol>
</code></pre>

> > ou en modifiant le fichier :  ".git/config" en ajoutant :
 <pre><code>
[branch "master"]
  remote = origin
  merge = refs/heads/master
</code><pre>

> Cyce de vie d'un fichier dans Git :
> > untracked -> unmodified -> modified -> staged -> commited -> pushed -> synchronised
<pre><code>
untracked : n'est pas suivi par Git (fichiers dans gitignore)
unmodified : n'est pas modifié
modified : modifié
staged : indexé pour le prochain commit
commited : les modifications indexés sont enregistrées dans le dépot local
pushed : publié sur le dépot distant
synchronised : fusionné avec la version du dépot distant (pulled)
</code></pre>

> Les commandes les plus communes :
<pre><code>
git add . (indexer les fichiers modifiés pour le prochain commit - stage modified files)
git commit -m "msg" (commit les modifications indéxées dans le depot local - commit staged files)
git push (publier les commits locaux sur le dépot distant)
git pull (récupèrer et fusionner automatiquement une branche distante avec une branche locale)
git pull --allow-unrelated-histories  (forcer la récupération)
</code></pre>


> Les branches Git :
<pre><code>
git branch < nom-branche > (créer une nouvelle branche avec le nom-branche)
git checkout < nom-branche > (switcher vers la branche nom-branche)
git merge < nom-branche > (fusionner la branche nom-branche avec la branche en cours)
git branch -d < nom-branche > (supprimer locallement la branche nom-branche)
git push origin --delete < nom-branche > (supprimer la branche distante nom-branche)
</pre></code>